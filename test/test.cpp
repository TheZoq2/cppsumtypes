#include "../src/sum_type.hpp"

#include <iostream>

int main() {
    auto fnbatch = FnBatch {
        [](int arg) {
            std::cout << "Int version" << std::endl;
            return 5;
        },
        [](float arg) {
            std::cout << "Float version" << std::endl;
            return 6;
        }
    };
    // fnbatch((int)5);
    // fnbatch((float)3.3);
    // return_type_of(
    //     TypeContainer<
    //         std::function<int(int)>,
    //         std::function<int(float)>,
    //         std::function<float(char*)>
    //     >{},
    //     (int)5,
    //     (float)3.3,
    //     (char*)"test"
    // );

    SumType<int, float> a{(int)5};

    // a.visit(fnbatch);
}

#pragma once

#include<variant>
#include<type_traits>
#include<algorithm>

#include "util.hpp"
#include "return_type_of.hpp"

/*
// TODO: Use std::forward and other reference stuff
template<typename R, typename ...Fs>
struct FnBatch : std::function<R(Fs)>... {
    public:
        FnBatch(std::function<R(Fs)>... fs) : std::function<R(Fs)>{fs}... {}
        using std::function<R(Fs)>::operator()...;
};
template<typename R, typename ...Fs>
auto make_batch(std::function<R(Fs)>... fs) -> FnBatch<R, Fs...> {
    return FnBatch(fs...);
}
*/

template <class ...Fs>
struct FnBatch : Fs... {
    explicit FnBatch() {};
    FnBatch(Fs&& ...fs) : Fs{std::forward<Fs>(fs)}...
    {}

    using Fs::operator()...;
};
template <class ...Ts>
FnBatch(Ts&&...) -> FnBatch<std::remove_reference_t<Ts>...>;


template <typename F, typename ...Fs, typename ...Ts>
constexpr void visit_helper();


template<typename ...Ts>
class SumType {
    public:
        template<typename I,
            typename X = typename std::enable_if<(std::is_same_v<I, Ts> || ...)>::type
        >
        SumType(I val) : inner(val) {
        }
        template<
            typename I,
            typename X = typename std::enable_if<!(std::is_same_v<I, Ts> || ...)>::type
        >
        SumType(I, ...) {
            static_assert(always_false<I>::value, "Can't construct from non-member");
        }

        template<
            typename F,
            typename ...Fs,
            typename R = std::invoke_result<F, typename TFirst<Ts...>::t>
        >
        R visit(FnBatch<Fs...> fns) {
            // visit_helper<Fs..., Ts...>();
            // return std::visit(fns, inner);
        }
    private:
        std::variant<Ts...> inner;
};


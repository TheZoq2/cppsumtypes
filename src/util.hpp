#pragma once

template<class T> struct always_false : std::false_type {};

template<class Expected, class Actual> struct unexpected_type : std::false_type {};


template<class T, class ...Ts> struct TFirst {
    using t = T;
};

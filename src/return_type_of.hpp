#include "util.hpp"

template<typename ...Ts>
struct TypeContainer {};

// Base case, only one function and parameter left
template<
    typename RExpected,
    typename F,
    template <typename...> typename Fns,
    typename T,
    typename R = std::invoke_result_t<F, T>,
    typename = typename std::enable_if<std::is_same_v<RExpected, R>>::type
>
constexpr RExpected return_type_of(Fns<F> f, T) {
}

// Base case. With failure
template<
    typename RExpected,
    typename F,
    template <typename...> typename Fns,
    typename T,
    typename R = std::invoke_result_t<F, T>,
    typename = typename std::enable_if<!std::is_same_v<RExpected, R>>::type
>
constexpr RExpected return_type_of(Fns<F>, T, ...) {
    static_assert(
        unexpected_type<RExpected, R>::value,
        "Not all functions return the same value"
    );
}

// Recursion helper, Defined if the return type of the current 
// function matches the expected value
template<
    typename RExpected,
    typename F,
    typename... FnTypes,
        template <typename...> typename Fns,
    typename T,
    typename... Ts,
    typename R = std::invoke_result_t<F, T>,
    typename = typename std::enable_if<std::is_same_v<RExpected, R>>::type
>
constexpr RExpected return_type_of(Fns<F, FnTypes...> f, T, Ts... ts) {
    return return_type_of<RExpected>(Fns<FnTypes...>{}, ts...);
}
// Recursion helper, for the failure case. Uses variadic function for low priority
template<
    typename RExpected,
    typename F,
    typename... FnTypes,
        template <typename...> typename Fns,
    typename T,
    typename... Ts,
    typename R = std::invoke_result_t<F, T>,
    typename = typename std::enable_if<!std::is_same_v<RExpected, R>>::type
>
constexpr RExpected return_type_of(Fns<F, FnTypes...> f, T, Ts... ts, ...) {
    static_assert(
        unexpected_type<RExpected, R>::value,
        "Not all functions return the same value"
    );
}

template<
    typename F,
    typename... FnTypes,
        template <typename...> typename Fns,
    typename T,
    typename... Ts,
    typename R = std::invoke_result_t<F, T>
>
constexpr R return_type_of(Fns<F, FnTypes...> f, T, Ts... ts) {
    return return_type_of<R>(Fns<FnTypes...>{}, ts...);
}

